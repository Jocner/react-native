// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
// import { getAnalytics } from "firebase/analytics";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyAYnu1AaopwsPLQjcXU_b57ivDbC60tbBo",
  authDomain: "tenedores-8291e.firebaseapp.com",
  projectId: "tenedores-8291e",
  storageBucket: "tenedores-8291e.appspot.com",
  messagingSenderId: "877387214402",
  appId: "1:877387214402:web:b675d4590fee2a39d207db",
  measurementId: "G-JM2BP7XYRM"
};

// Initialize Firebase
export const initFirebase = initializeApp(firebaseConfig);
// const analytics = getAnalytics(app);